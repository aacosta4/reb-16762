const fs = require('fs');
const Ajv = require("ajv");

const {
  CONFIG_GLOBAL_OUTAGE_SCHEMA,
  CONFIG_GLOBAL_NO_OUTAGE_SCHEMA
} = require('./constants');
const {
  getJsonFileData,
  checkFileExistence
} = require('./utils');

const ajv = new Ajv({
  strict: true,
  allErrors: true
});

const SCHEMAS = {
  OUTAGE: ajv.compile(CONFIG_GLOBAL_OUTAGE_SCHEMA),
  NO_OUTAGE: ajv.compile(CONFIG_GLOBAL_NO_OUTAGE_SCHEMA)
};

function validateFile(schema, filePath) {

  if (!Object.keys(SCHEMAS).find(key => key === schema)){
    throw new Error(`Invalid schema ${schema}! Expected OUTAGE or NO_OUTAGE!`);
  }

  const validate = SCHEMAS[schema];

  if (!checkFileExistence(filePath)) {
    throw new Error(`${JSON.stringify(data)} NOT FOUND!`);
  }

  const data = getJsonFileData(filePath);

  if (typeof data !== 'object') {
    console.error(`Current content:\n ${JSON.stringify(data)}`);
    throw new Error(`Expected ${filePath} content to be a JSON!`);
  }

  const valid = validate(data);
  if (!valid) {
    console.error(validate.errors);
    throw new Error(`Schema validation on ${filePath} has FAILED!`);
  } else console.log(`${filePath} schema validation has PASSED!`);
}

const schema = process.argv[2];

if (process.argv.length === 4) {
  const schema = process.argv[2].toUpperCase();
  const filePath = process.argv[3];
  validateFile(schema, filePath);
} else {
  console.log(
    `
Invalid command. Please use one of the following:
node validateOutageSchema.js {schema:OUTAGE or NO_OUTAGE} {filePath}
`
  );
}