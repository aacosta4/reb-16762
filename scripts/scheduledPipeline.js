const {
  parseISO,
  format
} = require('date-fns');
const fetch = require('axios').default;

const {
  CI_GITLAB_SCHEDULED_PIPELINE_TOKEN,
  CI_GITLAB_BRANCH,
  CI_GITLAB_TIME_ZONE,
  GITLAB_SCHEDULED_PIPELINE_PREFIX,
  GITLAB_API_URL,
} = require('./constants');

const BASE_HEADER = {
  "PRIVATE-TOKEN": CI_GITLAB_SCHEDULED_PIPELINE_TOKEN
};

/**
 * Retrieve current automated scheduled pipelines.
 */
// [START getAutomatedScheduledPipelines]
async function getAutomatedScheduledPipelines() {
  console.log('Fetching Scheduled Pipelines ...');
  return fetch({
    url: `${GITLAB_API_URL}/pipeline_schedules`,
    headers: BASE_HEADER
  }).then(
    ({
      data
    }) =>
    (data || [])
    .filter(({
      description
    }) => description.indexOf(GITLAB_SCHEDULED_PIPELINE_PREFIX) === 0)
    .map(({
      id
    }) => id)
  ).then(ids => {
    if (ids.length > 0) {
      console.log('Scheduled Pipelines FOUND!');
      console.log(ids);
    } else {
      console.log('Scheduled Pipelines NOT FOUND!');
    }
    return ids;
  });
}
// [END getAutomatedScheduledPipelines]

/**
 * Cancel scheduled pipeline.
 */
// [START deleteScheduledPipeline]
async function deleteScheduledPipeline(id) {
  return fetch({
    method: 'DELETE',
    url: `${GITLAB_API_URL}/pipeline_schedules/${id}`,
    headers: BASE_HEADER
  }).then(() => true);
}
// [END deleteScheduledPipeline]

/**
 * Clean scheduled pipelines.
 */
// [START cleanScheduledPipeline]
async function cleanScheduledPipelines() {
  const scheduledPipelines = await getAutomatedScheduledPipelines();

  if (scheduledPipelines instanceof Array &&
    scheduledPipelines.length > 0) {
    scheduledPipelines.map(id => console.log(`Job ${id} found.`));
    const requestResult = scheduledPipelines.map(id => deleteScheduledPipeline(id));
    await Promise.all(requestResult);
    console.log('Scheduled Pipelines CLEANED!');
  } else {
    console.log('NO Scheduled Pipelines to CLEAN!');
  }
}
// [END cleanScheduledPipeline]

/**
 * Post a scheduled pipeline.
 */
// [START postScheduledPipeline]
async function postScheduledPipeline(type, date) {
  console.log('Preparing Scheduled Pipeline ...');
  const cron = format(new Date(date), "mm HH dd MM *");
  console.log(`Date ${date} parsed to ${cron}`);
  console.log('Creating Scheduled Pipeline ...');
  return fetch({
    method: 'POST',
    url: `${GITLAB_API_URL}/pipeline_schedules`,
    headers: BASE_HEADER,
    data: {
      description: `${GITLAB_SCHEDULED_PIPELINE_PREFIX} ${type} ON ${date}` + Date.now(),
      ref: CI_GITLAB_BRANCH,
      cron,
      cron_timezone: CI_GITLAB_TIME_ZONE
    }
  }).then(({
    data: {
      id
    }
  }) => {
    console.log('Scheduled Pipeline CREATED!');
    return id;
  });
}
// [END postScheduledPipeline]

/**
 * Adds a variable for a scheduled pipeline.
 */
// [START postScheduledPipelineVariable]
async function postScheduledPipelineVariable(pipelineId, {
  name,
  value
}) {
  console.log('Preparing Scheduled Pipeline parameters ...');
  console.log(`'${name}' = '${value}'`);
  console.log('Adding Scheduled Pipeline parameter ...');
  return fetch({
    method: 'POST',
    url: `${GITLAB_API_URL}/pipeline_schedules/${pipelineId}/variables`,
    headers: BASE_HEADER,
    data: {
      "key": name,
      "value": value,
      "variable_type": "env_var",
    }
  }).then(() => console.log('Scheduled Pipeline paramter ADDED!'));
}
// [END postScheduledPipelineVariable]

/**
 * Creates a scheduled pipeline.
 */
// [START postScheduledPipelineVariable]
async function createScheduledPipeline(type, date) {
  const pipelineId = await postScheduledPipeline(type, date);
  await postScheduledPipelineVariable(pipelineId, {
    name: 'CI_MODE',
    value: type
  });
  console.log('Scheduled Pipeline creation COMPLETED!');
}
// [END postScheduledPipelineVariable]

const action = process.argv[2];

if (action && action === 'get') {
  getAutomatedScheduledPipelines();
} else if (action && action === 'delete' && process.argv.length >= 4) {
  const id = process.argv[3];
  deleteScheduledPipeline(id);
} else if (action && action === 'clean') {
  cleanScheduledPipelines();
} else if (action && action === 'create' && process.argv.length >= 5) {
  const type = process.argv[3];
  const date = process.argv[4];
  createScheduledPipeline(type, date);
} else {
  console.log(
    `
Invalid command. Please use one of the following:
node scheduledPipeline.js get
node scheduledPipeline.js delete {ID}
node scheduledPipeline.js clean
node scheduledPipeline.js schedule
node scheduledPipeline.js create {type} {date}
`
  );
}