// firebaseIntegration.js
const CI_FIREBASE_PARAMETER = process.env.CI_FIREBASE_PARAMETER || 'authenticated_outage';

// scheduledPipeline.js
const CI_GITLAB_PROJECT_ID = process.env.CI_GITLAB_PROJECT_ID;
const CI_GITLAB_SCHEDULED_PIPELINE_TOKEN = process.env.CI_GITLAB_SCHEDULED_PIPELINE_TOKEN;
const CI_GITLAB_BRANCH = process.env.CI_GITLAB_BRANCH || 'main';
const CI_GITLAB_TIME_ZONE = process.env.CI_GITLAB_TIME_ZONE || 'America/Denver'; // 'America/Sao_Paulo';
const GITLAB_SCHEDULED_PIPELINE_PREFIX = 'AUTOMATED::';
const GITLAB_API_URL = `https://gitlab.com/api/v4/projects/${CI_GITLAB_PROJECT_ID}`;

// smsIntegration.js
const CI_TWILIO_SID = process.env.CI_TWILIO_SID;
const CI_TWILIO_AUTH = process.env.CI_TWILIO_AUTH;
const CI_TWILIO_PHONE = process.env.CI_TWILIO_PHONE;
const CI_SMS_INTEGRATION_CONTACT_LIST = process.env.CI_SMS_INTEGRATION_CONTACT_LIST || './configs/contactList.json';

// validateOutageSchema.js
const ISO_8601_BASE = "^\\d{4}-\\d\\d-\\d\\dT\\d\\d:\\d\\d:\\d\\d(\\.\\d+)?(([+-]\\d\\d:\\d\\d)|Z)?$";

const CONFIG_GLOBAL_OUTAGE_SCHEMA = {
  type: "object",
  properties: {
    "systemDown": {
      type: "boolean"
    },
    "outageTitle": {
      type: "string"
    },
    "outageMessage": {
      type: "string"
    },
    outageActionButton: {
      type: "object",
      properties: {
        "url": {
          type: "string"
        },
        "text": {
          type: "string"
        },
      }
    },
    durationOfOutage: {
      type: "object",
      properties: {
        "startDateTime": {
          type: "string",
          pattern: ISO_8601_BASE
        },
        "endDateTime": {
          type: "string",
          pattern: ISO_8601_BASE
        },
      }
    },
    "testerUserIdHashes": {
      type: "string"
    }
  },
  required: ["systemDown", "outageTitle", "outageMessage", "durationOfOutage", "testerUserIdHashes"],
  additionalProperties: false
};

const CONFIG_GLOBAL_NO_OUTAGE_SCHEMA = {
  type: "object",
  properties: {
    "systemDown": {
      type: "boolean"
    },
    "outageTitle": {
      type: "string"
    },
    "outageMessage": {
      type: "string"
    },
    outageActionButton: {
      type: "object",
      properties: {
        "url": {
          type: "string"
        },
        "text": {
          type: "string"
        },
      }
    },
    durationOfOutage: {
      type: "object",
      properties: {
        "startDateTime": {
          type: "string",
          pattern: ISO_8601_BASE
        },
        "endDateTime": {
          type: "string",
          pattern: ISO_8601_BASE
        },
      }
    },
    "testerUserIdHashes": {
      type: "string"
    }
  },
  required: ["systemDown", "outageTitle", "outageMessage", "durationOfOutage", "testerUserIdHashes"],
  additionalProperties: false
};

module.exports = {
  // firebaseIntegration.js
  CI_FIREBASE_PARAMETER,

  // scheduledPipeline.js
  CI_GITLAB_PROJECT_ID,
  CI_GITLAB_SCHEDULED_PIPELINE_TOKEN,
  CI_GITLAB_BRANCH,
  CI_GITLAB_TIME_ZONE,
  GITLAB_SCHEDULED_PIPELINE_PREFIX,
  GITLAB_API_URL,

  // smsIntegration.js
  CI_TWILIO_SID,
  CI_TWILIO_AUTH,
  CI_TWILIO_PHONE,
  CI_SMS_INTEGRATION_CONTACT_LIST,

  // validateOutageSchema.js
  ISO_8601_BASE,
  CONFIG_GLOBAL_NO_OUTAGE_SCHEMA,
  CONFIG_GLOBAL_OUTAGE_SCHEMA
};