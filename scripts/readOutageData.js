const fs = require('fs');
const {
  getJsonFileData
} = require('./utils');

function readField(filePath, fieldName) {
  const configData = getJsonFileData(filePath);
  const value = configData.durationOfOutage[fieldName];
  return value.trim()
}

const action = process.argv[2];
const filePath = process.argv[3];

if (action && action === 'start' && !!filePath) {
  console.log(`startDateTime = ${readField(filePath, "startDateTime")}`);
} else if (action && action === 'end' && !!filePath) {
  console.log(`endDateTime = ${readField(filePath, "endDateTime")}`);
} else {
  console.log(
    `
Invalid command. Please use one of the following:
node readOutageData.js start {filePath}
node readOutageData.js end {filePath}
`
  );
}

//eval "export test=$(node scripts/readOutageData.js start | sed "s/^startDateTime = \(.*\)$/\1/")"