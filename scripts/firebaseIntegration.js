const fs = require('fs');
const admin = require('firebase-admin');
admin.initializeApp();

const {
  CI_FIREBASE_PARAMETER,
} = require('./constants');

/**
 * Get a valid access token.
 */
// [START retrieve_access_token]
// function getAccessToken() {
//   return admin.credential.applicationDefault().getAccessToken()
//       .then(accessToken => {
//         return accessToken.access_token;
//       })
//       .catch(err => {
//         console.error(err);
//         throw new Error('Unable to get access token');
//       });
// }
// [END retrieve_access_token]

/**
 * Retrieve the current Firebase Remote Config template from the server. Once
 * retrieved the template is stored locally in a file named `config.json`.
 */
// [START retrieve_template]
function getTemplate() {
  const config = admin.remoteConfig();
  config.getTemplate()
    .then(template => {
      console.log('ETag from server: ' + template.etag);
      const templateStr = JSON.stringify(template);
      fs.writeFileSync('config.json', templateStr);
    })
    .catch(err => {
      console.error(err);
      throw new Error('Unable to get template');
    });
}
// [END retrieve_template]

/**
 * Publish the local template stored in `config.json` to the server.
 */
// [START publish_template]
function publishTemplate() {
  const config = admin.remoteConfig();
  const template = config.createTemplateFromJSON(
    fs.readFileSync('config.json', 'utf-8'));
  config.publishTemplate(template)
    .then(updatedTemplate => {
      console.log('Template has been published');
      console.log('ETag from server: ' + updatedTemplate.etag);
    })
    .catch(err => {
      console.error(err);
      throw new Error('Unable to publish template.');
    });
}
// [END publish_template]

// [START update_template]
async function getAndUpdateTemplate(filePath) {
  const config = admin.remoteConfig();
  try {
    // Get current active template.
    console.log('Getting Firebase Remove Config template ...');
    const template = await config.getTemplate();

    console.log('Reading new config data ...');
    const rawOutageConfigData = fs.readFileSync(filePath);
    const outageConfigDataParsed = JSON.stringify(JSON.parse(rawOutageConfigData)) || "{}";

    console.log('Updating new config data ...');
    template.parameters[CI_FIREBASE_PARAMETER] = {
      defaultValue: {
        value: outageConfigDataParsed
      }
    };

    // Validate template after updating it.
    console.log('Validating new config data ...');
    await config.validateTemplate(template);
    // Publish updated template.
    const updatedTemplate = await config.publishTemplate(template);
    console.log('Latest etag: ' + updatedTemplate.etag);
    console.log('Firebase Remove Config has been updated!');
  } catch (err) {
    console.error(err);
    throw new Error('Unable to get and update template.');
  }
}
// [END update_template]

const action = process.argv[2];

if (action && action === 'get') {
  getTemplate();
} else if (action && action === 'publish') {
  publishTemplate();
} else if (action && action === 'update' && process.argv.length >= 4) {
  const filePath = process.argv[3];
  getAndUpdateTemplate(filePath);
} else {
  console.log(
    `
Invalid command. Please use one of the following:
node firebaseIntegration.js get
node firebaseIntegration.js publish
node firebaseIntegration.js update {filePath}
`
  );
}