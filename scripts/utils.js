const fs = require('fs');

function getJsonFileData(filePath) {
  let fileData = null;
  try {
    fileData = JSON.parse(fs.readFileSync(filePath));
  } catch (err) {
    console.error(err);
  } finally {
    return fileData;
  }
}

function checkFileExistence(filePath) {
  let fileExists = false;
  try {
    fileExists = fs.existsSync(filePath);
  } catch (err) {
    console.error(err);
  } finally {
    return fileExists;
  }
}

module.exports = {
  getJsonFileData,
  checkFileExistence
};