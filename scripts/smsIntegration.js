const twilio = require('twilio');
const {
  CI_TWILIO_SID,
  CI_TWILIO_AUTH,
  CI_TWILIO_PHONE,
  CI_SMS_INTEGRATION_CONTACT_LIST
} = require('./constants');
const {
  getJsonFileData
} = require('./utils');

const client = new twilio(CI_TWILIO_SID, CI_TWILIO_AUTH);

/**
 * Read contacts list from ./contactList.json file
 */
// [START getContacts]
function getContacts() {
  const contactsData = getJsonFileData(CI_SMS_INTEGRATION_CONTACT_LIST);

  const {
    smsContacts
  } = contactsData || {};

  if (!smsContacts) return [];

  const contacts = Object.keys(smsContacts)
    .map(name => ({
      name,
      phones: smsContacts[name]
    }));

  return contacts || [];
}
// [START getContacts]

/**
 * Send SMS through Twilio API
 */
// [START sendSms]
function sendSms(message, {
  name,
  to
}) {
  console.log(`Sending message to [${name}, ${to}]: ${message}`);
  client.messages
    .create({
      body: message,
      to, // Text this number JANSEN
      from: CI_TWILIO_PHONE, // From a valid Twilio number
    })
    .then(({
      sid
    }) => console.log(`Message Sent: ${sid}`));
}
// [END sendSms]

/**
 * Send SMS to contacts specified in contactList.json file
 */
// [START sendSmsToContacts]
function sendSmsToContacts(message) {
  const contacts = getContacts();
  contacts.forEach(
    ({
      name,
      phones
    }) =>
    phones.forEach(
      to => sendSms(message, {
        name,
        to
      })
    )
  );
}
// [END sendSmsToContacts]

const action = process.argv[2];

if (action && action === 'send') {
  const message = process.argv.length >= 4 ? process.argv[3] : "";
  sendSmsToContacts(message);
} else {
  console.log(
    `
Invalid command. Please use one of the following:
node smsIntegration.js send {message}
`
  );
}