# Authenticated Outage

## Parameters
1. CI_FIREBASE_PARAMETER (Default: authenticated_outage) => This parameter keeps the parameter name which is going to be updated in Firebase Remote Config;
2. CI_GITLAB_PROJECT_ID => This project id which is necessary to Gitlab APIs used to handle the scheduled pipelines;
3. CI_GITLAB_SCHEDULED_PIPELINE_TOKEN => Gitlab access token with API grant used to handle the scheduled pipelines;
5. CI_GITLAB_TIME_ZONE (Default: America/Denver) => Timezone for the scheduled pipeline (https://api.rubyonrails.org/classes/ActiveSupport/TimeZone.html);
6. GITLAB_SCHEDULED_PIPELINE_PREFIX (Default: 'AUTOMATED::') => Prefix used to identify the scheduled pipelines created by automation;
7. GITLAB_API_URL (Default: `https://gitlab.com/api/v4/projects/${CI_GITLAB_PROJECT_ID}`) => Gitlab API URL;
8. CI_TWILIO_SID => Twilio SID for SMS integration;
9. CI_TWILIO_AUTH => Twilio Authentication Token for SMS integration;
10. CI_TWILIO_PHONE => Twilio Phone Number for SMS integration;
11. CI_SMS_INTEGRATION_CONTACT_LIST (Default: './configs/contactList.json') => File path to contacts list;
11. SA_REBANK_OUTAGES: It's a CI/CD Variable, DevOps handles it, type FILE which stores the Google Service Account file content in base 64 (https://firebase.google.com/docs/remote-config/automate-rc#get_an_access_token_to_authenticate_and_authorize_api_requests);
12. GOOGLE_APPLICATION_CREDENTIALS => This variable is created during the pipeline running, based on the SA_REBANK_OUTAGES;
     ```bash
      - export GOOGLE_APPLICATION_CREDENTIALS=$(mktemp)
      - base64 -d $SA_REBANK_OUTAGES > $GOOGLE_APPLICATION_CREDENTIALS
     ```
13. CI_OUTAGE_CONFIG_FILE (Default: "./configs/outage.json"): File path which contains the OUTAGE specification;
14. CI_NO_OUTAGE_CONFIG_FILE (Default: "./configs/noOutage.json"): File path which contains the NO OUTAGE specification;
13. CI_MODE (Default: VALIDATE_OUTAGE_CONFIG) => It's used to handled the actions of the automated outage. Possible values:
    ```bash
      - VALIDATE_OUTAGE_CONFIG  # Triggers the schema validation of the OUTAGE and NO OUTAGE files;
      - SCHEDULE_OUTAGE         # Creates a scheduled pipeline which will TURN ON the outage;
      - TURN_ON_OUTAGE          # Push the OUTAGE configuration to Firebase and creates a scheduled pipeline which will TURN OFF the outage;
      - TURN_OFF_OUTAGE         # Push the NO OUTAGE configuration to Firebase and creates a scheduled pipeline which will TURN OFF the outage;
      - TEST_NOTIFICATION       # Triggers the SMS integration submitting an SMS test to the list of contacts in the file specified through the variable CI_SMS_INTEGRATION_CONTACT_LIST;
    ```

<br>

## Manual TRIGGERS
  ### There are 3 scheduled pipelines to handle actions manually:
  - MANUAL:: SCHEDULE_OUTAGE => It schedules your outage based on the _outage.json_ config data; It creates an "AUTOMATED:: TURN_ON_OUTAGE" scheduled pipeline;
  - MANUAL:: TURN_ON_OUTAGE => IT STARTS THE OUTAGE IMMEDIATELY based on the _outage.json_ config data BUT IGNORING durationOfOutage.startDateTime field data; It creates an "AUTOMATED:: TURN_OFF_OUTAGE" scheduled pipeline;
  - MANUAL:: TURN_OFF_OUTAGE => IT ENDS THE OUTAGE IMMEDIATELY on the _noOutage.json_ config data;

<br>

## How to Schedule an Outage
1. Set up your outage changing the file _outage.json_ specifying when it starts, ends and what features are impacted;
2. Commit and merge your changes to master;
3. Check if your pipeline has passed successfully;
4. Trigger the scheduled pipeline "MANUAL:: SCHEDULE_OUTAGE";
4. Once the "MANUAL:: SCHEDULE_OUTAGE" pipeline is completed, you should find a new scheduled pipeline called "AUTOMATED:: TURN_ON_OUTAGE";

<br>

## How to anticipate or start manually an Outage
1. Make sure your _noOutage.json_ is correctly set up;
2. If you already have your "AUTOMATED:: TURN_ON_OUTAGE" scheduled pipeline, you can just click on Run button;
3. If you don't have it, you can start the outage manually triggering "MANUAL:: TURN_ON_OUTAGE" pipeline clicking on Run button;

<br>

## How to end earlier or manually an Outage
1. Make sure your _noOutage.json_ is correctly set up;
2. If you already have your "AUTOMATED:: TURN_OFF_OUTAGE" scheduled pipeline, you can just click on Run button;
3. If you don't have it, you can start the outage manually triggering "MANUAL:: TURN_OFF_OUTAGE" pipeline clicking on Run button;

<br>

## Notifications
The notifications are sent when the pipeline starts, ends or fails but to get it you need to setup your phone number.

### Setup notifications
1. Add your name and phone numbers to _configs/contactList.json_ file;
2. Go to Scheduled Pipelines and run "MANUAL:: TEST_SMS_NOTIFICATION" scheduled pipeline to make sure you didn't mistype your phone number;
3. Once the pipeline completes you should receive a SMS with the message "NOTIFICATION TEST::You got a notification test.".

<br>

## Config Schemas

### config/outage.json
```javascript
{
  "systemDown": true,
  "outageTitle": "System unavailable",
  "outageMessage": "From 12am to 5:30am on Sunday, February 21, online and mobile banking (including the ATB Personal beta) will be unavailable while we take care of some routine maintenance.",
  "outageActionButton":{
    "url":"https://atbonline.com",
    "text":"Go to atbonline.com"
  },
  "durationOfOutage": {
    "startDateTime": "2021-09-21T00:00:00-07:00",
    "endDateTime": "2021-09-21T05:30:00-07:00"
  },
  "testerUserIdHashes": "7E83AF9E99FE0C47EFFF782EF04188DC35B6003E1F8197B1CA5DD70F1446A864,AA9701A5912E7B3A569850B13C28E7F3874420D45F876C2E4838732A9469CD87,3E440AF2D73C83EF5F4288D1A27BCAC6912C061D93A3A0229CEAA860F38B9EE7,A1B671EE61CA7B9FAFB1E4F8C664B9C967BE95E090C6F458AFA80671FEF07DCC,be2f3a5bf8b5ec5f9393671afe9a876fad7aa06c5558986090c153f70e4ed61b,dcde86e541b4d923d3d1bf03994aedad8266156b02dcd46314e735fb4f22c4f7,f5e5348461fbfce1966ad9a97c2c8c1b2c7be0f355bd8d0a5b651a3ba50542b0"
}
```

### config/noOutage.json
```javascript
{
  "systemDown": true,
  "outageTitle": "System unavailable",
  "outageMessage": "From 12am to 5:30am on Sunday, February 21, online and mobile banking (including the ATB Personal beta) will be unavailable while we take care of some routine maintenance.",
  "outageActionButton":{
    "url":"https://atbonline.com",
    "text":"Go to atbonline.com"
  },
  "durationOfOutage": {
    "startDateTime": "2021-09-21T00:00:00-07:00",
    "endDateTime": "2021-09-21T05:30:00-07:00"
  },
  "testerUserIdHashes": "7E83AF9E99FE0C47EFFF782EF04188DC35B6003E1F8197B1CA5DD70F1446A864,AA9701A5912E7B3A569850B13C28E7F3874420D45F876C2E4838732A9469CD87,3E440AF2D73C83EF5F4288D1A27BCAC6912C061D93A3A0229CEAA860F38B9EE7,A1B671EE61CA7B9FAFB1E4F8C664B9C967BE95E090C6F458AFA80671FEF07DCC,be2f3a5bf8b5ec5f9393671afe9a876fad7aa06c5558986090c153f70e4ed61b,dcde86e541b4d923d3d1bf03994aedad8266156b02dcd46314e735fb4f22c4f7,f5e5348461fbfce1966ad9a97c2c8c1b2c7be0f355bd8d0a5b651a3ba50542b0"
}
```

- `systemDown`: true/false - `true` blocks users from accessing login controls - i.e. username/password textboxes, Login button, etc
- `outageTitle`: The title of the message
- `outageMessage`: The message to display
- `outageActionButton`: Button/Link to launch url in external browser (optional)
- `durationOfOutage`: Object for timeframe to display outage message (optional). If omitted outage message will display immediately (as long as `systemDown=true`)
  - `startDateTime`: When to start showing the message
  - `endDateTime`: When to stop showing the message
- `testerUserIdHashes`: The SHA256 of the users allowed to bypass the outage by long pressing on the ATB logo (approx 10 seconds). Can use this tool to generate hash of login username https://emn178.github.io/online-tools/sha256.html

### config/contactList.json
```javascript
{
  "smsContacts": {
    "Dev1": ["+19999999999", "+19999999999"],
    "Dev2": ["+19999999999"],
  }
}
```